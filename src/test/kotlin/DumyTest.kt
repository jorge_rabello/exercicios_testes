import org.junit.Assert.assertEquals
import org.junit.Test

class DumyTest {

    @Test
    fun `should sum and return 10`() {
        val x = 5
        val y = 5
        assertEquals(10, x + y)
    }
}
